<?php

$workDays = array(1,2,3,4,5); // mon-fri

if(in_array(date('w'), $workDays)) {
	
	function doPost($url, $postData = array()) {
		$ch = curl_init($url);
		
		$cookieFile = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'cookies.txt';
		curl_setopt($ch, CURLOPT_COOKIEJAR, $cookieFile);
		curl_setopt($ch, CURLOPT_COOKIEFILE, $cookieFile);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
		curl_setopt($ch, CURLOPT_REFERER, "http://www.siteloom.dk/");
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1667.0 Safari/537.36');
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_TIMEOUT, 99999);
		$response = curl_exec($ch);
		$info = curl_getinfo($ch);	
		curl_close($ch);
		return $response;
	}
	
	// LOGIN
	$loginResponse = doPost('http://www.siteloom.dk/intranet/login/', array('siteloom_login_username' => 'Sessingø', 'siteloom_login_password' => 'SiteLoomSessingø01'));
	
	if($loginResponse) {
		
		// Change work status
		$workTimeResponse = doPost('http://www.siteloom.dk/velkommen/', array('worktimeAction' => 'true'));
		
		echo (strstr($workTimeResponse, 'Så er dagen begyndt') !== FALSE) ? 'Du har tjekket ind' : 'Du har tjekket ud';
		die();
	}

}